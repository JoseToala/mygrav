---
title: 'Framewords de Frontend'
---

¿Qué es Frontend?
Frontend es la parte de un programa o dispositivo a la que un usuario puede acceder directamente. Son todas las tecnologías de diseño y desarrollo web que corren en el navegador y que se encargan de la interactividad con los usuarios.

HTML, CSS y JavaScript son los lenguajes principales del Frontend, de los que se desprenden una cantidad de frameworks y librerías que expanden sus capacidades para crear cualquier tipo de interfaces de usuarios.

ALGUNOS DE ELLOS PUEDEN SER:

* React
* Redux
* Angular
* Bootstrap
* Foundation
* LESS
* Sass
* Stylus
* PostCSS