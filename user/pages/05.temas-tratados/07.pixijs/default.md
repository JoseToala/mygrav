---
title: PixiJS
media_order: pixijs.png
---

![](pixijs.png)PixiJS es una biblioteca de renderización que te permitirá crear gráficos interactivos, aplicaciones multiplataforma y juegos sin tener que sumergirte en la API de WebGL o lidiar con la compatibilidad del navegador y el dispositivo.

PixiJS tiene soporte completo para WebGL y recae sin problemas en el lienzo de HTML5 si es necesario. Como marco, PixiJS es una herramienta fantástica para crear contenido interactivo, especialmente al alejarse de Adobe Flash en los últimos años . Úselo para sus sitios web, aplicaciones interactivas y juegos HTML5 con muchos gráficos. Fuera de la caja La compatibilidad multiplataforma y la degradación elegante significan que tiene menos trabajo que hacer y se divierte más al hacerlo. Si desea crear experiencias refinadas y refinadas con relativa rapidez, sin ahondar en un código denso y de bajo nivel, al mismo tiempo que evita los dolores de cabeza de las inconsistencias del navegador, ¡entonces esparza su próximo proyecto con algo de magia PixiJS!

Mas información visite: <p><a href="https://www.pixijs.com/"><strong>PixiJS</strong></a></p>