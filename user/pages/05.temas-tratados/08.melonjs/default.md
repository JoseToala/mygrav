---
title: MelonJS
media_order: melonJS.png
---

melonJS es un motor de juegos HTML5 de código abierto que permite a los desarrolladores y diseñadores centrarse en el contenido.

El marco proporciona una colección de entidades compuestas y soporte para una serie de herramientas de terceros. Dándole una poderosa combinación que puede ser usada al por mayor o por partes.

![](melonJS.png)
