---
title: 'Elevator Saga'
media_order: elevator.PNG
---

¡Este es un juego de programación!

Su tarea es programar el movimiento de ascensores, escribiendo un programa en JavaScript. El objetivo es transportar a las personas de manera eficiente. Dependiendo de lo bien que lo hagas, puedes progresar a través de los desafíos cada vez más difíciles.
Solo los mejores programas podrán completar todos los desafíos.
![](elevator.PNG)
