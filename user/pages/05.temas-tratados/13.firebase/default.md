---
title: Firebase
---

Firebase es la nueva y mejorada plataforma de desarrollo móvil en la nube de Google. Se trata de una plataforma disponible para diferentes plataformas (Android, iOS, web), con lo que de esta forma presentan una alternativa seria a otras opciones para ahorro de tiempo en el desarrollo como Xamarin.

#### Compila apps rápido, sin administrar la infraestructura
Firebase te proporciona funciones como estadísticas, bases de datos, informes de fallas y mensajería, de manera que puedas ser más eficiente y enfocarte en tus usuarios.
#### Con el respaldo de Google y la confianza de apps reconocidas
Firebase utiliza la infraestructura de Google y se escala automáticamente, incluso para las apps más grandes.
#### Una plataforma con productos que funcionan mejor en conjunto
Los productos de Firebase funcionan a la perfección por sí solos. Sin embargo, debido a que comparten datos y estadísticas, funcionan aún mejor juntos.