---
title: 'FLAT FILE CMS'
---

Los flat file CMS son una variante relativamente nueva de los sistemas de gestión de contenidos que se erige como competidora para los CMS tradicionales como WordPress, Typo3 o Drupal. Estos sistemas se basan en los llamados flat files o archivos planos, es decir, archivos con una estructura muy sencilla, y plantean tanto ventajas como inconvenientes con respecto a los grandes rivales. A continuación explicamos cómo funcionan los CMS de archivos planos y detallamos los diferentes sistemas que ya están disponibles.

Algunos de los conocidos son los siguientes 

* Kiryby
* HTMLy
* Grav
* Razor
* WonderCMS