---
title: 'Technology Radar'
---

El Radar es un documento que establece los cambios que creemos que son relevantes actualmente en el desarrollo de software — cosas en movimiento a las que creemos que deberían prestar atención y considerar aplicar en sus proyectos. Refleja la opinión idiosincrásica de un grupo de tecnólogos de alto nivel y está basado en nuestras experiencias y trabajo cotidiano. Si bien pensamos que esto resulta interesante, no debería tomarse como un análisis profundo del mercado.

Puedes encontrar muchas mas informacion visitando: <p><a href="https://www.thoughtworks.com"><strong>TechnologyRadar</strong></a></p>