---
title: GDevelop
media_order: gdvelop.png
---

GDevelop es un creador de juegos de código abierto y multiplataforma diseñado para ser utilizado por todos, sin necesidad de conocimientos de programación.

Lo que hace que GDevelop sea único y tan fácil de usar son los eventos. Los eventos son una forma poderosa de expresar la lógica de tu juego, sin tener que aprender un lenguaje de programación.

GDevelop lleva la programación visual al siguiente paso, lo que le permite agregar comportamientos confeccionados a los objetos de su juego y crear nuevos comportamientos utilizando estos eventos intuitivos y fáciles de aprender.

![](gdvelop.png)