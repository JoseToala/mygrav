---
title: Phaser
media_order: phaser.png
---

![](phaser.png) Phaser es un marco de juego de código abierto HTML5 creado por Photon Storm. Está diseñado para crear juegos que se ejecutarán en navegadores web de escritorio y móviles. Se dio mucha importancia al rendimiento dentro de los navegadores web móviles, un área cada vez más importante de los juegos web. Si el dispositivo es capaz, a continuación, utiliza WebGL para la representación, pero de lo contrario, vuelve a la transparencia a Canvas. En este artículo, voy a cubrir los recursos para el aprendizaje de Phaser, así como mostrar lo que puede hacer y algunos de los pensamientos que entraron en su diseño.