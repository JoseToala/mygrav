---
title: SVG
media_order: svg.png
---

![](svg.png)SVG (Scalable Vector Grafics) significa Vectores Gráficos Escalables. Dicho así tal vez no lo entendamos, pero lo entendremos mejor si decimos que es una aplicación para hacer dibujos, banners, gráficos, etc. tanto estáticos como animados.

SVG es fácil de usar, ya que se basa en el lenguaje XML, es decir el mismo lenguaje de etiquetas que usa HTML. Esto significa que no tenemos que usar (en principio) javascript ni otro lenguaje diferente del HTML para incorporar figuras SVG en la página, ya que el XML es el mismo lenguaje de base que utilizan tanto HTML como SVG. Podemos por tanto crear las figuras y dibujos con SVG mediante etiquetas que incorporamos al lenguaje HTML.