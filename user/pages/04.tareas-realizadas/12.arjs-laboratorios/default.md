---
title: ARJS-Laboratorios
media_order: 'pattern-Lab1.png,pattern-Lab2.png,pattern-Lab3.png'
---

Para utilizar la realidad virtual debes ingresar al siguiente link <p><a href="https://overloadprime.com/appi3/applicationar/index.html"><strong>Link ARJS</strong></a></p>

Imagenes para poder utilizar la App

<center>LAB1</center>
![](pattern-Lab1.png)
<center>LAB2</center>
![](pattern-Lab2.png)
<center>LAB3</center>
![](pattern-Lab3.png)