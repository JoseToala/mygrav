---
title: 'Análisis de los SPA'
---

<h2>Introducción</h2>
Single-Page Application (SPA) es un formato que se esta popularizando con gran rapidez en los últimos meses debido a que mejora y unifica la experiencia de usuario, independientemente del dispositivo de acceso que se este utilizando. En este tipo de aplicaciones web, todo lo que se muestra y se este procesando se mostrara dentro de la misma página, así que pasar de una acción a otra no hace falta recargar el navegador ya que normalmente es un archivo en el que se recarga todo.

La manera más natural de crear un SPA es con Javascript, apoyándose en diferentes librerías y frameworks para facilitar su desarrollo: AngularJS, React, Vuejs.

<h2>AngularJS</h2>
Es un framework MVC de JavaScript para el desarrollo Web Front End que permite crear aplicaciones SPA, fáciles de comprobar y mantener. El marco de AngularJS consiste en un conjunto de herramientas bien integradas que te posibilitan la forma de poder construir aplicaciones del lado de cliente, bien estructuradas en un sistema modular con menos código y flexibilidad. AngularJS extiende HTML, proporcionando directrices que añaden funcionalidad al margen de beneficio y le permite al usuario crear sus propias directrices, incluso poder elaborar componentes reusables que muestran las necesidades del usuario y abstrayendo toda la lógica de manipulación del DOM.

AngularJS también implementa binding de datos de dos vías, conectando tu HTML (vistas) a los objetos de Javascript (modelos) sin problemas. En términos simples, quiere decir que cualquier actualización del modelo que se este realizando se reflejara inmediatamente en tu vista, sin necesidad de ningún tipo de manipulación DOM o control de eventos.

<h2>VueJS</h2>

VueJS es un framework progresivo para construir interfaces de usuario. A diferencia de otros frameworks monolíticos, VueJS esta diseñado desde el inicio para ser adoptado incrementalmente. Su biblioteca principal se enfoca solo en la capa de la vista, y es muy simple de utilizar e integrar con diferentes proyectos o cualquier biblioteca existente, incluso también es perfectamente capaz de soportar aplicaciones sofisticadas de una sola pagina cuando se implementan herramientas modernas y librerias compatibles.

El core principal de VueJS esta formado por una librería encargada de renderizar vistas en el navegador. Su forma de organizar el código es por medio de pequeños componentes que contienen todo HTML, CSS y JavaScript necesario para funcionar como pieza independiente. Estas piezas se van componiendo en un árbol jerárquico de componentes hasta formar cualquier aplicación que requiera el usuario. Utilizar las librerías son muy sencillas y contiene pasos bien simples para los usuarios.

<h2>ReactJS</h2>

React es una biblioteca escrita en JavaScript, desarrollada en Facebook para facilitar la creación de componentes interactivos, reutilizables, para interfaces de usuario. Normalmente se utiliza en Facebook para la producción de componentes, incluso Instagram está completamente escrito en React. Uno de sus puntos más destacados, es que no sólo se utiliza en el lado del cliente, sino que también se puede representar en el servidor, y trabajar juntos para una mayor interacción de usuario/cliente.

React esta formado en torno a hacer funciones, que toman actualizaciones de estado de la pagina y que se visualizan en una representación virtual de la pagina resultante. Siempre que React es informado de un cambio de estado, vuelve a ejecutar esas funciones para determinar una nueva representación virtual de la página, a continuación, se traduce automáticamente ese resultado en los cambios del DOM necesarios para reflejar la nueva presentación de la página.

A primera vista, esto suena como que fuera más lento que el enfoque JavaScript habitual de actualización de cada elemento, según sea necesario. Detrás de escena, sin embargo, React.js hace justamente eso: tiene un algoritmo muy eficiente para determinar las diferencias entre la representación virtual de la página actual y la nueva. A partir de esas diferencias, hace el conjunto mínimo de cambios necesarios en el DOM.

<h2>Análisis</h2>

En base a todas las investigaciones dadas, se puede realizar un análisis comparativo entre VueJS y ReactJS. AngularJS queda descartado debido a que su funcionalidad es muy avanzada y requiere de muchos más conocimientos y conceptos para poder tener un trabajo bien hecho con esta herramienta, es muy buena, pero con bastantes requerimientos en él.

Resulta un poco difícil hacer una comparación con ReactJS y VueJS ya que ambos son totalmente eficientes y ofrecen características venerables para que el trabajo del desarrollador sea mucho más agiles y fáciles al momento de crear aplicaciones web. Ambos marcos son reconocidos por proporcionar altos rendimientos y son mucho más fáciles de aprender.

Además, ambos marcos se emplean ampliamente para diseñar las interfaces de usuario y también se utilizan para trabajar con la biblioteca raíz. Inclusive tienen similitud en reactividad y estructura basada en componentes.

<h2>Concluciones</h2>

Los tres frameworks mencionados son muy buenos para cualquier desarrollador de servicios web, sin embargo, cada uno tiene sus expectativas y diferencias al momento de trabajarlos y ejecutarlos, siendo cada uno muy buenos competidores entre sí.

Si se desea crear una aplicación grande con facilidad de desarrollo es mejor irse por la opción de utilizar ReactJS, debido a sus especialidades y herramientas que ofrece para este campo, por otra parte, si el usuario esta destinado a construir una aplicación sencilla con la mejor documentación y fácil integración es mejor optar por VueJS.

Para mi seria mucho más conveniente trabajar con ReactJS por la fácil interacción de las herramientas que proporciona con el usuario, tiene una documentación bien detallada que ayuda mucho para el entendimiento de este framework y sus complementos y librerías ayudan a tener un buen conocimiento y cualquier aplicación web que realicemos será mucho mas estructurada y conveniente usando esta herramienta.

