---
title: Juegos
cache_enable: false
---

Juego con PixiJs

Consiste en encontrar la pareja de la carta para ganar.

<p><a href="https://overloadprime.com/juegos/concentration2/"><strong>Juego con Pixijs</strong></a></p>

Juego con Phaser

Consiste en conseguir las monedas sin chocar con los picos.

<p><a href="https://overloadprime.com/juegos/phasergame/t_hunter.html"><strong>Phaser</strong></a></p>