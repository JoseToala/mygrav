---
title: 'Documentacion Del Proyecto'
---

## Aplicación con ReactJS implementando realidad Aumentada con ARJS para los Laboratorios
El proyecto que se va a realizar estara con las siguientes funcionalidades:

ReactJS que sera nuestro framework principal para la parte de la vista de nuestro proyecto, yo elegi reactJS debido a su facilidad de comunicacion e infraestructura que tiene con el usuario, incluso siendo el mas completo y perfecto para el diseño de este proyecto.

Firebase que utilizare para recorrer su facilidad de conexcion con react, siendo parte fundamental del diseño de este proyecto, ya que define los parametros necesarios para la creacion de una base de datos estable, ademas de que dentro de la base de datos de firebase podemos utilizar lo que es la autenticacion de usuarios, ponerle reglas a nuestro favor.

ARJS que es la parte de la realidad aumentada donde se mostraran los diferentes docentes que esten dentro de ese laboratorio por medio de marcadores .patt.