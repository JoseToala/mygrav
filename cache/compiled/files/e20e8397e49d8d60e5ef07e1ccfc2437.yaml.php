<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/grav/user/plugins/tntsearch/languages.yaml',
    'modified' => 1560352058,
    'data' => [
        'en' => [
            'PLUGIN_TNTSEARCH' => [
                'FOUND_RESULTS' => 'Found %s results',
                'FOUND_IN' => 'in <span>%s</span>'
            ]
        ]
    ]
];
