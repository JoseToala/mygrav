<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://highlight/highlight.yaml',
    'modified' => 1560352054,
    'data' => [
        'enabled' => true,
        'theme' => 'default',
        'lines' => false
    ]
];
