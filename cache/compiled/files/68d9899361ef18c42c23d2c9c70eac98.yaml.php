<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://antimatter/antimatter.yaml',
    'modified' => 1560352109,
    'data' => [
        'enabled' => true,
        'dropdown' => [
            'enabled' => false
        ]
    ]
];
