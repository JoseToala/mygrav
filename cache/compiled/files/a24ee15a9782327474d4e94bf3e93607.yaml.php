<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/grav/user/config/plugins/ganalytics.yaml',
    'modified' => 1560379520,
    'data' => [
        'enabled' => true,
        'trackingId' => 'UA-139852008-1',
        'position' => 'head',
        'objectName' => 'ga',
        'forceSsl' => false,
        'async' => false,
        'anonymizeIp' => false,
        'cookieConfig' => false,
        'cookieName' => '_ga',
        'cookieExpires' => 63072000,
        'debugStatus' => false,
        'debugTrace' => false
    ]
];
